import React, { Component } from 'react';

// import LightBox from 'react-images';
import Carousel, { Modal, ModalGateway } from 'react-images';

class ImageLigthBox extends Component {
    state = {
        lightboxIsOpen: true,
        currentImage: this.props.pos,
        images: []
    }

    static getDerivedStateFromProps(props, state) {
        if (props.images) {
            const images = [];
            let str = `{`;
            props.images.forEach(element => {
                str += `src: ${element},`;
                // images.push({src: `${element}`})
            });
            str += `}`;
            images.push(str);
            return state = {
                images
            }
        }
        return false;
    }

    closeLightBox = () => {
        this.props.onclose();
    }
    render() {
        console.log(this.state.images)
        return (
            // <LightBox 
            //     currentImage={this.state.currentImage}
            //     images={this.state.images}
            //     isOpen={this.state.lightboxIsOpen}
            //     onClickPrev={() => this.gotoPrevious()}
            //     onClickNext={() => this.gotoNext()}
            //     onClose={() => this.closeLightBox()}
            // />
            <ModalGateway>
            {this.state.lightboxIsOpen ? (
              <Modal onClose={this.closeLightBox()}>
                <Carousel views={this.state.images} />
              </Modal>
            ) : null}
          </ModalGateway>
        );
    }
}

export default ImageLigthBox;