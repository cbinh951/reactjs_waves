import React from 'react';
import Cart from './cart';

const CartBlock = (props) => {

    const renderCards = () => (
        props.list ? props.list.map((card, i) => (
                <Cart 
                    key={i}
                    {...card}
                />
        )) : null
    )
    
    return (
        <div className="card_block">
            <div className="container">
                {
                    props.title ?
                        <div className="title">
                            {props.title}
                        </div>
                    :null
                }
                <div style={{
                    display:'flex',
                    flexWrap:'wrap'
                }}>
                    { renderCards(props.list)}
                </div>

            </div>
        </div>
    );
};

export default CartBlock;
