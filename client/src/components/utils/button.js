import React from 'react';
import {Link} from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';

const MyButton = (props) => {

    const buttons = () => {
        let templates = '';
        switch (props.type) {
            case "default":
                templates = <Link 
                                className={!props.altClass ? "link_default" : props.altClass} 
                                to={props.linkTo} 
                                {...props.addStyles}
                            >
                                {props.title}
                            </Link>;
            break;
            case "bag_link":
                templates = <div className="bag_link"
                    onClick={() => {
                        props.runAction();
                    }}
                >   
                    <FontAwesomeIcon 
                        icon={faShoppingBag}
                    />
                </div>
            break;
            case "add_to_cart_link":
                templates = 
                    <div className="add_to_cart_link"
                        onClick={() => {
                            props.runAction();
                        }}
                    >
                        <FontAwesomeIcon 
                            icon={faShoppingBag}
                        />
                        Add to cart
                    </div>
            break;
            default:
                templates = '';
                break;
        }
        return templates;
    }

    return (
        <div className="my_link">
            {buttons()}
        </div>
    );
};

export default MyButton;
