import React, { Component } from 'react';

import HomeSlider from './home_slider';
import HomePromotion from './home_promotion';
import CartBlock from '../utils/cart_block';

import { connect } from 'react-redux';
import {getProductsBySell, getProductsByArrival} from '../../actions/product_actions';

class Home extends Component {

    componentDidMount() {
        this.props.dispatch(getProductsBySell());
        this.props.dispatch(getProductsByArrival());
    }

    render() {
        return (
            <div>
                <HomeSlider />
                <CartBlock 
                    list={this.props.products.bySell}
                    title="Best Selling guitars"
                />
                <HomePromotion />
                <CartBlock 
                    list={this.props.products.byArrival}
                    title="New arrivals"
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.products
    }
};

export default connect(mapStateToProps)(Home);